#include "norskkalender.h"
#include <set>
#include <iostream>
#include <iterator>
#include <time.h>
#include <stdio.h>

NorskKalender::NorskKalender(int year)
{
	this->year = year;
	time_t paaskedag = getEasterDay();
	fridager.insert(getDate(1,1,year));
	fridager.insert(getDate(1,5,year));
	fridager.insert(getDate(17,5,year));
	fridager.insert(getDate(25,12,year));
	fridager.insert(getDate(26,12,year));
	fridager.insert(getDate(27,12,year));
	fridager.insert(getDate(28,12,year));
	fridager.insert(getDate(29,12,year));
	fridager.insert(getDate(30,12,year));
	fridager.insert(getDate(31,12,year));
	fridager.insert(getOffsetDate(paaskedag,-7));
	fridager.insert(getOffsetDate(paaskedag,-3));
	fridager.insert(getOffsetDate(paaskedag,-2));
	fridager.insert(getOffsetDate(paaskedag,0));
	fridager.insert(getOffsetDate(paaskedag,1));
	fridager.insert(getOffsetDate(paaskedag,39));
	fridager.insert(getOffsetDate(paaskedag,49));
	fridager.insert(getOffsetDate(paaskedag,50));
}

bool NorskKalender::erFridag(time_t dato)
{
	struct tm* tm = localtime(&dato);
	std::set<time_t>::iterator iterator;
	for (iterator = fridager.begin(); iterator != fridager.end(); ++iterator)
	{
		time_t tid = *iterator;
		struct tm* fd_tm = localtime(&tid);
    if (fd_tm->tm_mday == tm->tm_mday && fd_tm->tm_mon == tm->tm_mon) {
			return true;
		}
	}
	return false;
}

time_t NorskKalender::getEasterDay()
{
	int year = this->year;
	int a = year % 19;
	int b = year / 100;
	int c = year % 100;
	int d = b / 4;
	int e = b % 4;
	int f = (b + 8) / 25;
	int g = (b - f + 1) / 3;
	int h = ((19 * a) + b - d - g + 15) % 30;
	int i = c / 4;
	int k = c % 4;
	int l = (32 + (2 * e) + (2 * i) - h - k) % 7;
	int m = (a + (11 * h) + (22 * l)) / 451;
	int n = (h + l - (7 * m) + 114) / 31; // This is the month number.
	int p = (h + l - (7 * m) + 114) % 31; // This is the date minus one.

	int month = n - 1;
	int day = p + 1;

	return getDate(day, month, year);
}

time_t NorskKalender::getOffsetDate(time_t easterDayDate, int daysOffset)
{
   struct tm* tm = localtime(&easterDayDate);
	 tm->tm_mday += daysOffset;
	 return mktime(tm);
}

time_t NorskKalender::getDate(int day, int month, int year)
{
	char timedetails[21];
	sprintf(timedetails, "%d %d %d T 00:00:00", year, month, day);
  char *tp = timedetails;
  struct tm tm;
  strptime(tp, "%Y %m %d T %H:%M:%S", &tm);
  return mktime(&tm);
}

int NorskKalender::getYear()
{
	return this->year;
}
