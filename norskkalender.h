#ifndef norskkalender_h
#define norskkalender_h

#include <set>
#include <time.h>

class NorskKalender
{
  public:
    NorskKalender(int year);
    bool erFridag(time_t dato);
    int getYear();

  private:
    int year;
    std::set<time_t> fridager;
    time_t getEasterDay();
    time_t getOffsetDate(time_t easterDayDate, int daysOffset);
    time_t getDate(int day, int month, int year);
};

#endif
